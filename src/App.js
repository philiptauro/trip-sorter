import React, { Component } from 'react';
import './App.css';
import Results from './Results/Results.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onInputChange = this.onInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  getJSON(url, callback) {
    try{
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.responseType = 'json';
      xhr.onload = function() {
        var status = xhr.status;
        if (status === 200) {
          callback(null, xhr.response);
        } else {
          callback(status, xhr.response);
        }
      };
      xhr.send();
    }
    catch(e){
      alert("An error occurred");
      callback(null, xhr.response);
    }
  };
  readData(){
    var refClass= this;
    return new Promise(function(resolve, reject) {
        refClass.getJSON('./_data/response.json',
          function(err, data) {
            if (err !== null) {
              alert('Something went wrong: ' + err);
              reject();
            } else {
              resolve(data);
            }
        });
    });
  }

  filterDirectsAndVia(arr, _from, _to){
    let directs = arr.filter(function(elem){
      if (elem.departure === _from && elem.arrival === _to){
        return elem;
      }
    });
    let via = arr.filter(function(elem){
      if (elem.departure === _from && elem.arrival !== _to){
        return elem;
      }
    });
    return [directs, via];
  }

  getViaRoutes(arr, viaRoute, destination){
    let route = []
    route.push(viaRoute);
    let trail = [];
    while (viaRoute.departure != destination){
      if (trail.indexOf(viaRoute.departure) > 0){

      }
      else{
        trail.push(viaRoute.departure);
        let [directs, via] = this.filterDirectsAndVia(arr, viaRoute.departure, destination);
        console.log(directs);
      }
    }
  }

  getDuration(h, m){
    return (h * 60) + m;
  }

  filterRoutes(_from, _to, sort = "fastest"){
    let classRef = this;
    this.readData()
      .then(function(data){
        let [directs, via] = classRef.filterDirectsAndVia(data.deals, _from, _to);
        var fastestDir;
        var fastestVia;
        directs.map(function(direct){
          direct["durInMins"] = classRef.getDuration(parseInt(direct.duration.h), parseInt(direct.duration.m));
          if (!fastestDir){
            fastestDir = direct;
          }
          else{
            if (fastestDir["durInMins"] > direct["durInMins"]){
              fastestDir = direct;
            };
          }
        });
        let dirFastestTime = fastestDir["durInMins"];

        via.map(function(viaRt){
          viaRt["durInMins"] = classRef.getDuration(parseInt(viaRt.duration.h), parseInt(viaRt.duration.m));
          if (!fastestVia){
            fastestVia = viaRt;
          }
          else{
            if (fastestVia["durInMins"] > viaRt["durInMins"]){
              fastestVia = viaRt;
            };
          }
        });
        let viaFastestTime = fastestVia["durInMins"];



        if (sort == "fastest"){
          if (dirFastestTime < viaFastestTime){
            console.log(fastestDir);  
            classRef.setState({
              route: fastestDir
            });  
          }
          else{
            classRef.setState({
              route: fastestVia
            }); 
            document.getElementsByName("try")[0].textContent = "Fastest " + JSON.stringify(fastestVia);
            let [directs, via] = classRef.filterDirectsAndVia(data.deals, fastestVia.departure, _to);
            directs.map(function(direct){
              direct["durInMins"] = classRef.getDuration(parseInt(direct.duration.h), parseInt(direct.duration.m));
              if (!fastestDir){
                fastestDir = direct;
              }
              else{
                if (fastestDir["durInMins"] > direct["durInMins"]){
                  fastestDir = direct;
                };
              }
            });
            let dirFastestTime = fastestDir["durInMins"];

            via.map(function(viaRt){
              viaRt["durInMins"] = classRef.getDuration(parseInt(viaRt.duration.h), parseInt(viaRt.duration.m));
              if (!fastestVia){
                fastestVia = viaRt;
              }
              else{
                if (fastestVia["durInMins"] > viaRt["durInMins"]){
                  fastestVia = viaRt;
                };
              }
            });
            let viaFastestTime = fastestVia["durInMins"];


          }
        }


    });
  }
  onInputChange(event) {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value
    })
    this.checkSelects(event);
  }
  fastestRoute(){

  }
  checkSelects(event){
    let selectTagFrom = document.getElementsByName("from")[0]; 
    let selectTagTo = document.getElementsByName("to")[0];
    let btnSearch = (document.getElementsByName("btnSearch")[0]);

    let _from = selectTagFrom.value;
    let _to = selectTagTo.value;

    if (_from === _to) {
      btnSearch.setAttribute('disabled', 'disabled');
      alert("Departure and Destination Port can't be the same.");
      event.target.focus();
      return;
    }
    if (_from === "" || _to === "") {
      btnSearch.setAttribute('disabled', 'disabled');
    }
    else{
      btnSearch.removeAttribute('disabled');
    }
  }
  handleSubmit(event) {
    let selectTagFrom = document.getElementsByName("from")[0]; 
    let selectTagTo = document.getElementsByName("to")[0];
    let _from = selectTagFrom.value;
    let _to = selectTagTo.value;
    this.filterRoutes(_from, _to);
    event.preventDefault();
  }
  render() {
      return (
        <div className="App" >
            <h3>Trip Sorter</h3>
            <form onSubmit={this.handleSubmit}>
              <select name="from" value={this.state.from} onChange={this.onInputChange} required="required">
                <option value=""> Select Departure Port </option> 
                <option value="London"> London </option> 
                <option value="Paris"> Paris </option> 
                <option value="Moscow"> Moscow </option> 
                <option value="Amsterdam"> Amsterdam </option> 
                <option value="Madrid"> Madrid </option> 
              </select> 
              <br/><br/>
              <select name="to" value={this.state.to} onChange={this.onInputChange} required="required">
                <option value=""> Select Destination Port </option> 
                <option value="London"> London </option> 
                <option value="Paris"> Paris </option> 
                <option value="Moscow"> Moscow </option> 
                <option value="Amsterdam"> Amsterdam </option> 
                <option value="Madrid"> Madrid </option> 
              </select>
              <br/><br/>
              <button type="submit" name="btnSearch"  className="btn btn-success" disabled="disabled">Search</button>
            </form>
            <span name="try"> </span>
            <Results route={this.state.route}></Results>
      </div>);
  }
}

export default App;